# Design, fabrication and test of a Generic GNSS Module for PocketQubes
In this repository you will find all the files related to the Design, Fabrication and Test of a Generic GNSS Module for PocketQubes, which works with GPS, Galileo and GLONASS. In addition to complying with the official
PocketQube standard, it also complies with the PQ9 electrical standard, with the aim of facilitating its
implementation in different projects. The subsystem uses the SPI protocol for debugging and
communication with the picosatellite OBC.

The project faces in the design stage the different challenges posed by the characteristics of a
PocketQube: regulations, dimensions, weight, electromagnetic protection, precision, low
consumption, price, etc.

The final design is the result of an iterative prototyping process, which is based on the consecutive
performance of all relevant tests. These tests include, among others: analysis of the behaviour of the
GNSS antenna, verification of the correct operation of the data transmission, evaluation of the quality of the link, etc. These tests allow the complete evaluation and characterization of the designed system.

This repository contains the following files:

   * **Project report**: it describes in Spanish the process of designing, manufacturing and testing the prototype.
   * **PCB files**: contains the .sch and .pcb files of the two PCBs that make up the prototype and the list of materials for each one.
   * **STL files** for the exterior structure of the PocketQube.
   * Files of the **S11 measurements** of the GNSS antenna.
   * **#C code** to use the STM32 development board as a SPI-UART converter.